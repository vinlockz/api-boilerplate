const path = require('path');
const nodeExternals = require('webpack-node-externals');

const plugins = [];

module.exports = {
  mode: 'development',
  target: 'node',
  entry: {
    app: [
      './graphql/app/index.js',
    ],
  },
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: 'server.js',
  },
  externals: [nodeExternals()],
  plugins: plugins,
};