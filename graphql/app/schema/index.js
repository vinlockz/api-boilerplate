const { GraphQLSchema } = require('graphql');
const { applyMiddleware } = require('graphql-middleware');
const Query = require('./query');
const RootMutation = require('./mutations');

const errorMiddleware = async (resolve, root, args, context, info) => {
  try {
    return await resolve(root, args, context, info);
  } catch (err) {
    return {
      errorCode: err.message,
      errorDesc: err.errorDesc ? err.errorDesc : null,
      csCode: err.csCode ? err.csCode : null,
    };
  }
};

const schemaOptions = {};

if (Object.keys(Query._fields()).length > 0) {
  schemaOptions.query = Query;
}

if (Object.keys(RootMutation._fields()).length > 0) {
  schemaOptions.mutation = RootMutation;
}

const schema = new GraphQLSchema(schemaOptions);

module.exports = applyMiddleware(schema, errorMiddleware);
