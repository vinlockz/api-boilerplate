const { GraphQLObjectType } = require('graphql');

const RootMutation = new GraphQLObjectType({
  name: 'Mutations',
  description: 'Mutations',
  fields: () => ({})
});

module.exports = RootMutation;
