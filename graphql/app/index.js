const graphqlServer = require('./graphqlServer');

const {
  GRAPHQL_EXPRESS_POWERED_BY_DISABLED,
  GRAPHQL_PMX_ENABLED,
  GRAPHQL_URL_ENCODED_PARSER_ENABLED,
  GRAPHQL_JSON_PARSER_ENABLED,
  GRAPHQL_LOGGING_ENABLED,
  GRAPHQL_INTROSPECTION_DISABLED,
  GRAPHQL_EXPRESS_PORT,
  GRAPHQL_GRAPHIQL_ENABLED,
  REDIS_ENABLED,
  REDIS_HOST,
  REDIS_PORT,
  REDIS_CLUSTER,
  REDIS_ENABLE_SSL,
  GRAPHQL_ENABLED,
  DEBUG_MODE,
} = process.env;

const options = {};
if (GRAPHQL_EXPRESS_POWERED_BY_DISABLED === 'false') {
  options.disablePoweredBy = false;
}
if (GRAPHQL_PMX_ENABLED === 'true') {
  options.pmxEnabled = true;
}
if (GRAPHQL_URL_ENCODED_PARSER_ENABLED === 'true') {
  options.enableUrlEncodedParser = true;
}
if (GRAPHQL_JSON_PARSER_ENABLED === 'false') {
  options.enableJsonParser = false;
}
if (GRAPHQL_LOGGING_ENABLED === 'false') {
  options.enableLogging = false;
}
if (GRAPHQL_INTROSPECTION_DISABLED === 'false') {
  options.introspectionDisabled = false;
}
if (GRAPHQL_EXPRESS_PORT !== undefined && !isNaN(GRAPHQL_EXPRESS_PORT)) {
  options.port = Number(GRAPHQL_EXPRESS_PORT);
}
if (GRAPHQL_GRAPHIQL_ENABLED === 'false') {
  options.graphiqlEnabled = false;
}
if (DEBUG_MODE === 'true') {
  options.debugMode = true;
}
if (REDIS_ENABLED === 'true') {
  options.redisEnabled = true;
  options.redisHost = REDIS_HOST;
  options.redisPort = REDIS_PORT;
  options.redisCluster = REDIS_CLUSTER === 'true';
  options.redisEnableSSL = REDIS_ENABLE_SSL === 'true';
}

// Other Options here

if (GRAPHQL_ENABLED === 'true') graphqlServer(options);
