const Logger = require('abstract-winston-json');
const Redis = require('ioredis');
const hash = require('object-hash');
const _ = require('lodash');

let client = null;

const redisCacheMiddleware = (options) => {
  const defaults = {
    redisEnabled: false,
    redisHost: null,
    redisPort: 6379,
    redisCluster: false,
    redisEnableSSL: false,
    redisAdditionalConfig: {},
  };

  options = _.extend(defaults, options);

  const logger = new Logger();

  const middleware = [];

  if (options.redisEnabled) {
    let config = {
      port: options.redisPort,
      host: options.redisHost,
      reconnectOnError: (err) => {
        if (err.message.startsWith('READONLY')) {
          logger.warn('redis.log', 'Redis server returned a READONLY error, reconnecting.');
        }
        return 2;
      },
      retryStrategy: (times) => {
        logger.warn('redis.log', 'Lost Redis connection, reattempting.');
        return Math.min(times * 2, 2000);
      },
      ...options.redisAdditionalConfig,
    };

    if (options.redisEnableSSL) {
      config.auth_pass = '';
      config.tls = {
        checkServerIdentity: () => undefined,
      };
    }

    if (options.redisCluster) {
      client = new Redis.Cluster([config], {
        scaleReads: 'slave',
      });
    } else {
      client = new Redis(config);
    }

    client.on('ready', () => {
      logger.log('redis.log', 'Redis is ready.');
    });

    client.on('error', (err) => {
      logger.error('redis.error', err);
    });

    const redisMiddleware = (req, res, next) => {
      if (client.status === 'ready') {
        req.redis = client;
        return next();
      } else {
        client.on('ready', () => {
          req.logger.log('redis.log', 'Redis is ready.');
          req.redis = client;
          return next();
        });
      }
    };

    redisMiddleware.client = client;

    middleware.push(redisMiddleware);
  }

  middleware.push(function cacheMiddleware(req, res, next) {
    req.cache = async (key, fallback, expire = 10, isAsync = true) => {
        if (!options.redisEnabled && !Object.prototype.hasOwnProperty.call(req, 'redis')) {
          // Fallback since there is no redis.
          let response = null;
          if (isAsync) {
            response = await fallback();
          } else {
            response = fallback();
          }
          return response;
        }

      // If redis exists
      if (typeof key !== 'string' && typeof key === 'object') {
        key = hash(key);
      } else if (typeof key !== 'string' && typeof key !== 'object') {
        throw new TypeError('Invalid cache key type. Must be a string or an object.');
      }

      // Get cached response
      let response = await req.redis.get(key);

      // Log cached response
      req.logger.log('cache.getCache', { key, response });
      if (response === null) {
        // If there is no cached response, fallback and then set the new cached response.
        if (isAsync) {
          response = await fallback();
        } else {
          response = fallback();
        }

        // Asynchronously set the cache and log, do not need to await for it.
        req.logger.log('cache.setCache', { key, response });

        const stringifiedResponse = (typeof response === 'object') ? JSON.stringify(response) : response;

        req.redis.set(key, stringifiedResponse, 'EX', expire);
      } else {
        // Parse the JSON cache grab
        response = JSON.parse(response);
      }
      return response;
    };
    return next();
  });

  return middleware;
};

module.exports = redisCacheMiddleware;
