const uuid = require('uuid/v4');

const requestIdMiddleware = () => (req, res, next) => {
  req.serverRequestId = uuid();
  res.set({ 'x-server-request-id': req.serverRequestId });
  return next();
};

module.exports = requestIdMiddleware;
