const cors = require('cors');

const corsMiddleware = (validHosts = []) => async (req, res, next) => {
  return cors({
    origin: (origin, callback) => {
      if (validHosts.length === 0) {
        return callback(null, true);
      } else if (origin && validHosts.some(host => origin.endsWith(`.${host}`))) {
        return callback(null, true);
      }
      return callback(new Error('INVALID_ORIGIN'), false);
    },
    optionsSuccessStatus: 200,
    methods: [ 'POST' ],
  })(req, res, next);
};

module.exports = corsMiddleware;
