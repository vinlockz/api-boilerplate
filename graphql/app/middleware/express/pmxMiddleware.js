const pm2 = require('@pm2/io');

const { PMX_ENABLED } = process.env;

let pmxMiddleware = () => [];
if (PMX_ENABLED === 'true') {
  const io = pm2.init({
    http: true,
    errors: true,
    custom_probes: true,
    network: true,
    ports: false,
  });

  const probe = io.probe();

  const rpsMeter = probe.meter({
    name: 'Incoming Requests/sec',
    type: 'meter',
  });

  const requestOutPerSecond = probe.meter({
    name: 'Outgoing Requests/sec',
    type: 'meter',
  });

  pmxMiddleware = () => (req, res, next) => {
    req.pmx = {};
    req.pmx.requestOutMark = () => requestOutPerSecond.mark();
    rpsMeter.mark();
    return next();
  };
}

module.exports = pmxMiddleware;
