const Logger = require('abstract-winston-json');

const getIpAddress = (request) => {
  let ip = request.connection.remoteAddress;
  if (Object.prototype.hasOwnProperty.call(request.headers, 'x-forwarded-for')) {
    ip = request.headers['x-forwarded-for'].split(',')[0].trim();
  }
  return ip;
};

const logServerRequestId = (request) => {
  if (Object.prototype.hasOwnProperty.call(request, 'serverRequestId')) {
    request.logger.addMetadata('serverRequestId', request.serverRequestId);
  }
};

const logRequest = (request) => {
  if (Object.prototype.hasOwnProperty.call(request.headers, 'x-request-id')) {
    request.logger.addMetadata('clientRequestId', request.headers['x-request-id']);
  }

  request.logger.log('client.request', {
    headers: request.headers,
    method: request.method,
    params: request.params,
    query: request.query,
    body: request.body,
    path: request.path,
    signedCookies: request.signedCookies,
    originalUrl: request.originalUrl,
    protocol: request.protocol,
    baseUrl: request.baseUrl,
    ip: getIpAddress(request),
    route: request.route,
    secure: request.secure,
  });
};

const logResponse = (request, response) => {
  response.on('finish', function commitLogs() {
    request.logger.log('client.response', {
      body: response.body,
      headers: response.getHeaders(),
    });
  });
  const _write = response.write;
  const _end = response.end;
  const chunks = [];

  response.write = function newWrite(...args) {
    chunks.push(Buffer.from(args[0]));
    _write.apply(response, args);
  };

  response.end = function newEnd(...args) {
    if (args[0]) chunks.push(Buffer.from(args[0]));
    const body = Buffer.concat(chunks).toString('utf8');
    let json = null;
    try {
      json = JSON.parse(body);
    } catch (err) {
      json = body || null;
    }

    response.body = json;
    _end.apply(response, args);
  };
};

const logMiddleware = () => (req, res, next) => {
  req.logger = new Logger({}, process.env.DEBUG_MOD === 'true');

  // Log Request ID to Log MetaData
  logServerRequestId(req);

  // Log Request
  logRequest(req);

  // On Complete
  logResponse(req, res);

  return next();
};

module.exports = logMiddleware;
