require('dotenv').config();
require('./utils/polyfills');
const express = require('express');
const graphqlHTTP = require('express-graphql');
const bodyParser = require('body-parser');
const _ = require('lodash');
const requestIdMiddleware = require('./middleware/express/requestIdMiddleware');
const logMiddleware = require('./middleware/express/logMiddleware');
const pmxMiddleware = require('./middleware/express/pmxMiddleware');
const redisCacheMiddleware = require('./middleware/express/redisCacheMiddleware');
const corsMiddleware = require('./middleware/express/corsMiddleware');
const NoIntrospection = require('graphql-disable-introspection');
const dataloaders = require('./dataloaders');
const gracefulShutdown = require('../../utils/gracefulShutdown');
const Logger = require('abstract-winston-json');
const schema = require('./schema');

/**
 * GraphQL Server
 * @param {object} options Options Object
 * @param {boolean} [options.disablePoweredBy=true] Disabled X-Powered-By Response Header
 * @param {boolean} [options.pmxEnabled=false] Enable PMX Middleware
 * @param {boolean} [options.enableUrlEncodedParser=false] Enable URL Encoded Body Parser
 * @param {boolean} [options.enableJsonParser=true] Enable JSON Body Parser
 * @param {boolean} [options.enableLogging=true] Enable Logging
 * @param {boolean} [options.redisEnabled=false] Enable Redis Cache
 * @param {string} [options.redisHost=null] Redis Host
 * @param {number} [options.redisPort=6379] Redis Port
 * @param {boolean} [options.redisCluster=false] Is Redis Cluster?
 * @param {boolean} [options.redisEnableSSL=false] Redis SSL Enabled
 * @param {boolean} [options.introspectionDisabled=true] Disable GraphQL Introspection
 * @param {Array<function>} [options.formatErrorMiddleware=[]] Format Error Middleware
 * @param {boolean} [options.graphiqlEnabled=true] Enable GraphiQL
 * @param {Array<string>} [options.validCorsDomains=[]] Valid CORS Domains
 */
module.exports = (options) => {
  const defaults = {
    disablePoweredBy: true,
    pmxEnabled: false,
    enableUrlEncodedParser: false,
    enableJsonParser: true,
    enableLogging: true,
    redisEnabled: false,
    redisHost: null,
    redisPort: 6379,
    redisCluster: false,
    redisEnableSSL: false,
    redisAdditionalConfig: {},
    introspectionDisabled: true,
    port: 4000,
    formatErrorMiddleware: [],
    graphiqlEnabled: true,
    debugMode: false,
    validCorsDomains: [],
  };

  options = _.extend(defaults, options);

  // Start Express App
  const app = express();

  if (options.disablePoweredBy) {
    // Disable X-Powered-By Header
    app.disable('x-powered-by');
  }

  const middleware = [];

  // JSON Only Body Parser
  if (options.enableJsonParser) {
    middleware.push(bodyParser.json());
  }
  // URL Encoded Body Parser
  if (options.enableUrlEncodedParser) {
    middleware.push(bodyParser.urlencoded({extended: true}));
  }
  // Generate Request ID
  middleware.push(requestIdMiddleware());
  // Logging Middleware
  if (options.enableLogging) {
    middleware.push(logMiddleware());
  }
  // PMX Middleware
  if (options.pmxEnabled) {
    middleware.push(pmxMiddleware());
  }
  // Redis Cache Middleware
  if (options.redisEnabled) {
    middleware.push(redisCacheMiddleware({
      redisEnabled: options.redisEnabled,
      redisHost: options.redisHost,
      redisPort: options.redisPort,
      redisCluster: options.redisCluster,
      redisEnableSSL: options.redisEnableSSL,
      redisAdditionalConfig: options.redisAdditionalConfig,
    }));
  }
  // Cors Middleware
  if (options.validCorsDomains) {
    middleware.push(corsMiddleware());
  }

  // And all middleware
  app.post('/', middleware);

  // Test Route
  app.get(/^\/(test|ping)$/, function testRoute(req, res) {
    res.json({ ping: 'pong' });
  });

  // Disable Introspection in Production
  const validationRules = [];
  if (options.introspectionDisabled) {
    validationRules.push(NoIntrospection);
  }

  // Run GraphQL Query Processor
  app.use('/', graphqlHTTP(function graphqlOptions(request) {
    return {
      schema,
      validationRules,
      formatError: (err) => {
        options.formatErrorMiddleware.forEach((middleware) => {
          middleware(request, err);
        });
        if (request.logger) {
          request.logger.log('graphql.error', err);
        }
        return err;
      },
      graphiql: options.graphiqlEnabled,
      context: {
        request,
        dataloaders: dataloaders(request),
      },
    };
  }));

  // Start Listening
  const server = app.listen(options.port, () => {
    const logger = new Logger({}, options.debugMode);
    logger.log('server.start', {
      status: 'running',
      port: options.port,
    });
    logger.debug('debug.mode', {
      enabled: true,
    });
  });

  gracefulShutdown(server);
};
