const Logger = require('abstract-winston-json');

const connections = [];

const shutdown = (server) => () => {
  const logger = new Logger();
  logger.warn('server.warn', 'Recieved kill signal, shutting down gracefully.');
  server.on('close', () => {
    logger.log('server.notice', {
      message: 'Closed out connections',
    });
    process.exit(0);
  });

  setTimeout(() => {
    logger.error('server.error', {
      message: 'Could not close out connections in time, forcefully shutting down.',
    });
    process.exit(1);
  }, 10000);

  connections.forEach(curr => curr.end());
  setTimeout(() => {
    connections.forEach(curr => curr.destroy())
  }, 5000);
};

const gracefulShutdownListener = (server) => {
  // Manage Connections
  server.on('connection', (connection) => {
    connections.push(connection);
    connection.on('close', () => connections.filter(curr => curr !== connection));
  });

  // Shutdown gracefully
  process.on('SIGTERM', shutdown(server));
  process.on('SIGINT', shutdown(server));
};

module.exports = gracefulShutdownListener;
